Title: Kraken-R

Author Information ----------------------------------------------------------

AUTHOR: David Geeraerts
EMAIL: geeraerd@evergreen.edu
LOCATION: Olympia, Washington U.S.A.


Purpose ---------------------------------------------------------------------
Purpose: Kraken-R is a monolithic R script for learning R.
The script style is as a cheatsheet of sorts to help remember how to do
certain things, and strives to provide better examples than what is
available in R help or vignettes.
It uses The Evergreen State College,
Computer Applications Lab (CAL) for Scientific Computing,
HeadCount to learn R, as well as some other datasets.
Single file to keep it simple,
even though using Project Template & LCFD file Model is a good idea.
LCFD: Load, Clean, Function, Do --each as a seperate script.
[https://cran.r-project.org/]
See Project Template for more information:
[http://projecttemplate.net/]


Copyleft --------------------------------------------------------------------
Copyleft License, Creative Commons:
Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)
http://creativecommons.org/licenses/by-nc-sa/3.0/
